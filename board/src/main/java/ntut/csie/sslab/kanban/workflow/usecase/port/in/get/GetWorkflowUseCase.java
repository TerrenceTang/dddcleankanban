package ntut.csie.sslab.kanban.workflow.usecase.port.in.get;

import ntut.csie.sslab.ddd.usecase.UseCase;

public interface GetWorkflowUseCase extends UseCase<GetWorkflowInput, GetWorkflowOutput> {
}
