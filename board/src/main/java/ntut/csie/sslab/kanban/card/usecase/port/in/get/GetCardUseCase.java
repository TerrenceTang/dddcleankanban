package ntut.csie.sslab.kanban.card.usecase.port.in.get;

import ntut.csie.sslab.ddd.usecase.UseCase;

public interface GetCardUseCase extends UseCase<GetCardInput, GetCardOutput> {
}
