package ntut.csie.sslab.kanban.workflow.usecase.port.in.lane.swimlane.create;

import ntut.csie.sslab.ddd.usecase.cqrs.Command;
import ntut.csie.sslab.ddd.usecase.cqrs.CqrsCommandOutput;

public interface CreateSwimLaneUseCase extends Command<CreateSwimLaneInput, CqrsCommandOutput> {
}
