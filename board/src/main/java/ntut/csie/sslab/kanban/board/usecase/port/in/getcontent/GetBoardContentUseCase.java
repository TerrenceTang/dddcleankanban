package ntut.csie.sslab.kanban.board.usecase.port.in.getcontent;

import ntut.csie.sslab.ddd.usecase.UseCase;

public interface GetBoardContentUseCase extends UseCase<GetBoardContentInput, GetBoardContentOutput> {

}
